

public class CastingExample{

	public static void main(String[] args){

		//AUTOMATIC CASTING METHOD WIDENING 
		//DON'T BE GET CASTING FOT INT AND CHAR

		byte	bt		=2;
		short 	sh      =bt;
		//char
		int 	in		= sh;
		long 	lng		= in;
		float   flt	    = lng;
		double  db		= flt;

		//--------------------------------------


		double 		doble 		= 21.2d;
		float  		flotante 	= (float) doble;
		long  		largo  		= (long)  flotante;
		int         entero  	= (int)	  largo;
		short    	corto  		= (short) entero;
		byte        cortisimo  	= (byte)  corto;















		


		System.out.println("--------------AUTOMATIC WIDENING CASTING------------");
	    System.out.println("Byte     : "  + bt);
		System.out.println("Short    : "  + sh);
		System.out.println("Integer  : "  + in);
		System.out.println("Long     : "  + lng);
		System.out.println("Double   : "  + db);
		System.out.println("Float    : "  + flt);
		

		


		System.out.println("--------------MANUAL NARROWING CASTING ------------");
	    System.out.println("Double  : "  + doble);
		System.out.println("Float   : "  + flotante);
		System.out.println("Long    : "  + largo);
		System.out.println("Integer : "  + entero);
		System.out.println("Short   : "  + corto);
		System.out.println("Byte    : "  + cortisimo);
		System.out.println("-----------------------------------------------------------");

	}
}