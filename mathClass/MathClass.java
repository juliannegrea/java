
public class MathClass{

	public static void main(String[] args) {


		int x = 5;
		int y = 66;
		int z =-54;
		int ranNumbers = (int)(Math.random() * 101);


		//Math.max() find a heighest value

		Math.max(x,y);
		Math.min(x,y);
		Math.sqrt(x);
		Math.abs(z);
		


		System.out.println("-------------------------------------------------"); 

		System.out.println("HaighestValue is: " + Math.max(x,y));
			System.out.println("\n");

		

		//Math.min() find a lowest value
		System.out.println("LowestValue is: " + Math.min(x,y));
			System.out.println("\n");

		
		//Math.sqrt() return square root value
		System.out.println("SquareRootValue is: " + Math.sqrt(x));
			System.out.println("\n");


		//Math.abs() method returns the absolute (positive) value of z
		System.out.println("AbsolutePositiveValue is: " + Math.abs(z));
			System.out.println("\n");

		//Math.random() returns a random number between 0.0 (inclusive), and 1.0 (exclusive):
 		System.out.println("RandomNumbers 0-100 is: " + ranNumbers);



		System.out.println("-------------------------------------------------"); 






	
}

}